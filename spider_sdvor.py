from bs4 import BeautifulSoup
import requests
from typing import List
import time
import sqlite3
# SDVOR
HOST = 'sdvor.com'
CATALOG_WORD = 'moscow/category/'
DETAIL_WORD = 'moscow/product/'
FULL_HOST = 'https://www.' + HOST


def get_full_url(short_url: str) -> str:
    if short_url.find(HOST) < 0:
        return FULL_HOST + '/' + short_url
    else:
        return short_url


def cut_url(full_url: str) -> str:
    if full_url.find(HOST) >= 0:        
        parts = full_url.partition(HOST + '/')
        short_url = parts[2]
    else:
        short_url = full_url
        if short_url[0] == '/':
            short_url = short_url[1:]
    if not short_url:
        return ""        
    parts = short_url.partition('?')
    if str(parts[2]).find('page=') >= 0:
        parts = short_url.partition('&')
        return parts[0] if parts[0].find('page=') >= 0 else ""
    else:
        return parts[0]


def check_url_is_relevant(url: str) -> bool:
    if url.find(CATALOG_WORD) >= 0 or url.find(DETAIL_WORD) >= 0:
        return True
    return False


def check_url_is_directory(url: str) -> bool:
    if url.find(DETAIL_WORD) >= 0:
        return False
    return True        


def parse_links(url: str) -> List[str]:
    if not check_url_is_directory(url):
        return list()
    full_url = get_full_url(url)
    while True:
        try:
            result = requests.get(full_url)
            break
        except Exception:
            time.sleep(1)            
        
    html = result.text
    soup = BeautifulSoup(html, 'html.parser')
    time.sleep(1)
    links = []    
    for tag in soup.find_all('a'):
        new_url = str(tag.get('href'))
        if check_url_is_relevant(new_url):
            new_url = cut_url(new_url)
            if new_url:
                links.append(new_url)
    return links

def get_unhandled():
    cursor = URLS_DB.cursor()
    sql = ''' 
        SELECT url
        FROM urls
        WHERE handled = 0 AND detail = 0
    '''    
    return { row[0] for row in cursor.execute(sql) if row[0] }

def check_handled(url: str) -> bool:
    cursor = URLS_DB.cursor()  
    cursor.execute('''
        SELECT handled
        FROM urls
        WHERE url = ?
    ''', (url,))    
    result = cursor.fetchone()
    if not result:
        return False
    return True if result[0] == 1 else False

def new_url(url: str, detail=0):
    cursor = URLS_DB.cursor()
    try:
        cursor.execute('INSERT INTO urls VALUES(?, 0, ?)', (url, detail))
        URLS_DB.commit()
    except (sqlite3.DatabaseError, sqlite3.DataError):
        pass
        

def set_handled(url: str):
    cursor = URLS_DB.cursor()
    try:
        cursor.execute('UPDATE urls SET handled = 1 WHERE url = ?', (url,))
        URLS_DB.commit()
    except (sqlite3.DatabaseError, sqlite3.DataError):
        pass


def scan_base() -> int:    
    urls = get_unhandled()
    print(f'REMAINS: {len(urls)} RECORDS\n\n')
    counter = 0
    for url in urls:
        print(f'HANDLE: {url}')        
        children = parse_links(url)
        for child in children:
            if not check_handled(child):
                if check_url_is_directory(child):                    
                    new_url(child, 0)
                    print(f'NEW CHILD: {child}')
                    counter += 1
                else:
                    new_url(child, 1)
                    print(f'NEW_DETAIL: {child}')            
        set_handled(url)                

    return counter

def main():    
    global URLS_DB
    URLS_DB = sqlite3.connect('urls_sdvor.sqlite')
    try:
        cursor = URLS_DB.cursor()
        cursor.execute('''
            CREATE TABLE IF NOT EXISTS urls (
                url text PRIMARY KEY,
                handled integer DEFAULT 0,
                detail integer DEFAULT 0                
            )
        ''')
        counter = 1
        new_url(FULL_HOST, 0)
        while counter > 0:
            counter = scan_base()        
    finally:
        URLS_DB.close()

if __name__ == '__main__':    
    main()
    
