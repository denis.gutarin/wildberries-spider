from bs4 import BeautifulSoup
import requests
from typing import List
import time
import sqlite3
import zlib

HOST = 'wildberries.ru'
FULL_HOST = 'https://www.' + HOST


def get_full_url(short_url: str) -> str:
    if short_url.find(HOST) < 0:
        return FULL_HOST + '/' + short_url
    else:
        return short_url


def set_handled(url: str):
    cursor = URLS_DB.cursor()
    try:
        cursor.execute('UPDATE urls SET handled = 1 WHERE url = ?', (url,))
        URLS_DB.commit()
    except (sqlite3.DatabaseError, sqlite3.DataError):
        pass


def get_unhandled_details():
    cursor = URLS_DB.cursor()
    sql = ''' 
        SELECT url
        FROM urls
        WHERE handled = 0 AND detail = 1
    '''    
    return { row[0] for row in cursor.execute(sql) if row[0] }


def get_html(url: str) -> str:
    full_url = get_full_url(url)
    while True:
        try:
            result = requests.get(full_url, timeout=5)
            break
        except Exception:
            time.sleep(5)            
    time.sleep(0.5)    
    return result.text


def save_html(url: str, html: str):
    zhtml = zlib.compress(html.encode(), level=9)
    cursor = HTMLS_DB.cursor()
    try:
        cursor.execute('''
            INSERT INTO htmls
            VALUES(?, ?)
        ''', (url, zhtml))
        HTMLS_DB.commit()
    except (sqlite3.DatabaseError, sqlite3.DataError):
        pass


def scan_base() -> int:    
    urls = get_unhandled_details()
    print(f'REMAINS: {len(urls)} RECORDS\n\n')
    counter = 0
    all_count = len(urls)
    for url in urls:
        counter += 1
        print(f'FETCH {str(counter)} / {str(all_count)} :: {url}')        
        html = get_html(url)
        save_html(url, html)
        set_handled(url)                

    urls = get_unhandled_details()
    return len(urls)


def main():    

    global URLS_DB        
    URLS_DB = sqlite3.connect('urls_temp.sqlite')
    global HTMLS_DB
    HTMLS_DB = sqlite3.connect('htmls.sqlite')    

    counter = 1
    try:        
        cursor = HTMLS_DB.cursor()
        cursor.execute('''
            CREATE TABLE IF NOT EXISTS htmls (
                url text PRIMARY KEY,
                data BLOB NOT NULL
            )
        ''')
        while counter > 0:
            counter = scan_base()        
    finally:
        URLS_DB.close()
        HTMLS_DB.close()


if __name__ == '__main__':    
    main()
    
